
	SECTION	measurestr,CODE

	INCLUDE	"asmgle.i"

	;
	; Exported functions
	;

	XDEF	measure_strarray

	;
	; Imported variables/functions
	;

	XREF	_PixLength

;--------------------------------------------------------------
; Entry:	a0=pointer to array of string pointers
; Exit:	d0=number of strings in array
;	d1=PixLength of widest string in array

measure_strarray:
	movem.l	d2/a0-1,-(a7)

	move.l	a0,a1			;a1=pointer array
	moveq	#0,d1			;d1=widest string so far
	moveq	#0,d2			;d2=number of strings
.ms_widthloop:
	move.l	(a1)+,d0			;d0=ptr to next string
	beq.s	.ms_gotwidth

	addq.l	#1,d2			;Inc count of strings

	move.l	d0,a0			;Get width of current string
	bsr	_PixLength
	move.l	d0,d1			;d1=MAX(width, widestsofar)
	bra.s	.ms_widthloop

.ms_gotwidth:
	move.l	d2,d0			;d0=string count

	movem.l	(a7)+,d2/a0-1
	rts
