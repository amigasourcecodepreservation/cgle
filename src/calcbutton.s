
	SECTION	calcbutton,CODE

	INCLUDE	"asmgle.i"

	;
	; Exported functions
	;

	XDEF	_CalcButton

	;
	; Imported variables/functions
	;

	XREF	init_xgad
	XREF	calc_xlength
	XREF	_LabelPixLength

;--------------------------------------------------------------
; Entry:	d0=gadget index
;	a0=text for gadget
; Exit:	Button gadget calculated

_CalcButton:
	movem.l	d0-1/a0-1,-(a7)

	jsr	init_xgad		;Init x,y,height

	move.l	a0,XG_Label(a1)

	;Width = LabelPixLength (Label) + PixLength ("xx", 0);

	moveq	#0,d1
	bsr	_LabelPixLength
	move.w	d0,XG_Width(a1)

	moveq	#2,d0
	jsr	calc_xlength
	add.w	d0,XG_Width(a1)

	movem.l	(a7)+,d0-1/a0-1
	rts

