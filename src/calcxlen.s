
	SECTION	calcxlength,CODE

	INCLUDE	"asmgle.i"

	;
	; Exported functions
	;

	XDEF	calc_xlength


	;
	; Imported variables/functions
	;

	XREF.l	_PixLength

;--------------------------------------------------------------
; Get the width of a number of "x" characters
; Entry:	d0=number of "x"s to measure (0-7)
; Exit:	d0.w=width of x's

calc_xlength:
	movem.l	d1/a0-1,-(a7)

	move.l	d0,d1		;d1=scratch x count

	lea	.cx_cache(pc,d0.w),a1 ;d0=value from cache
	move.b	(a1),d0

	bpl.s	.cx_done		;Exit if already calc'd

	subq.l	#8,a7		;Create "x" string on stack
	subq.l	#1,d1
	move.l	a7,a0
.cx_initstr:	
	move.b	#"x",(a0)+
	dbf	d1,.cx_initstr
	clr.b	(a0)

	move.l	a7,a0		;Get length of x string
	moveq	#0,d1
	jsr	_PixLength

	move.b	d0,(a1)		;Store length in cache

	addq.l	#8,a7

.cx_done:	movem.l	(a7)+,d1/a0-1
	ext.w	d0
	rts

.cx_cache:
	dc.b 0	;Width of 0 x's (always 0!)
	dc.b -1	;Width of 1 x
	dc.b -1	;  "   "  2 x
	dc.b -1	;  "   "  3 x
	dc.b -1	;  "   "  4 x
	dc.b -1	;  "   "  5 x
	dc.b -1	;  "   "  6 x
	dc.b -1	;  "   "  7 x
	EVEN

