
	SECTION	calctext,CODE

	INCLUDE	"asmgle.i"

	;
	; Exported functions
	;

	XDEF	_CalcText
	XDEF	_CalcString

	;
	; Imported variables/functions
	;

	XREF	init_xgad
	XREF	calc_xlength
	XREF	_PixLength

;--------------------------------------------------------------
; Entry:	d0=gadget index
;	a0=text for gadget
; Exit:	Text gadget calculated

_CalcText:
_CalcString:
	movem.l	d0-1/a0-1,-(a7)

	jsr	init_xgad		;Init x,y,height

	;Width = PixLength (Text) + PixLength ("xx")
	
	moveq	#0,d1
	bsr	_PixLength
	move.w	d0,XG_Width(a1)

	moveq	#2,d0
	jsr	calc_xlength
	add.w	d0,XG_Width(a1)

	movem.l	(a7)+,d0-1/a0-1
	rts
