;
; macro definitions
;

GAD_MAXIMUM:	equ	100	; Maximum allowed gadgets

VERTICAL:		equ	1	; Initial window alignments
HORIZONTAL:	equ	0

WARN: 		equ	1	; Runtime non-fatal 
ERROR: 		equ	2	; Runtime fatal 
BUG:		equ	3	; Error in programming

;
; structure offsets
;

	RSRESET
PAN_Win:		rs.l 1		;Window for this panel
PAN_GList:	rs.l 1		;Gadget list
PAN_Title:	rs.l 1		;Window title string
PAN_WinX:		rs.l 1		;Window coords
PAN_WinY:		rs.l 1
PAN_Width:	rs.l 1		;Window size
PAN_Height:	rs.l 1
PAN_SIZEOF:	equ __RS


	RSRESET
XG_Gad:		rs.l 1		;Pointer to gadget structure
XG_Label:		rs.l 1		;Gadget label
		rs.w 1
XG_x:		rs.w 1		;Gadget position and size
		rs.w 1
XG_y:		rs.w 1
		rs.w 1
XG_Width:		rs.w 1
		rs.w 1
XG_Height:	rs.w 1
XG_Disabled:	rs.b 1		;0=enabled 1=disabled
		rs.b 1
XG_SIZEOF:	equ __RS

