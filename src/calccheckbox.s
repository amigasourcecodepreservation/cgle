
	SECTION	calccheckbox,CODE

	INCLUDE	"asmgle.i"

	;
	; Exported functions
	;

	XDEF	_CalcCheckbox

	;
	; Imported variables/functions
	;

	XREF	init_xgad
	XREF	calc_xlength
	XREF	_PixLength

;--------------------------------------------------------------
; Entry:	d0=gadget index
; Exit:	Checkbox gadget calculated

_CalcCheckbox:
	movem.l	d0-1/a0-1,-(a7)

	jsr	init_xgad			;Init x,y,height

	moveq	#3,d0			;Set width to "xxx"
	jsr	calc_xlength
	move.w	d0,XG_Width(a1)
	
	movem.l	(a7)+,d0-1/a0-1
	rts

