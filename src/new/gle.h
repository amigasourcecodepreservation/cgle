/*
 * Header file for gle.lib
*/

#ifndef GLE_HEADER_INCLUDED
#define GLE_HEADER_INCLUDED

#include <clib/graphics_protos.h>
#include <dos/dos.h>
#include <exec/nodes.h>
#include <devices/inputevent.h>
#include <graphics/gfx.h>

/*
 * macro definitions
*/

#define GAD_MAXIMUM	100		// Maximum allowed gadgets

#define	VERTICAL	1		// Initial window alignments
#define HORIZONTAL	0

#define WARN 		1		// A runtime non-fatal error occurred, something went wrong but program continues
#define ERROR 		2		// A runtime fatal error occurred, something went wrong and program stops
#define BUG			3		// An error was made while programming, program stops


#define MIN(a,b) ((a)<(b)?(a):(b))
#define MAX(a,b) ((a)>(b)?(a):(b))


/*
 * type definitions
*/

typedef struct PANEL 
{
	struct Window *Win;					/* Window for this panel */
	struct Gadget *GList;				/* Gadgets for this window */
	STRPTR Title;						/* Pointer to title string */
	LONG WinX, WinY;					/* Upper left corner of window */
	LONG Width, Height;					/* Size of this panel */
};

typedef struct EXT_GADGET 
{
	struct Gadget *Gad;
	char *Label;				// Text of the label of this gadget
	LONG x;						// Size and position of the gadget on screen
	LONG y;
	LONG Width;
	LONG Height;
	UBYTE Disabled;				// 0 = enabled, 1 = disabled
};


/*
 * function prototypes
*/

__asm BOOL WindowConstructor (void);
void WindowDestructor (void);

LONG RequestUserInput (char *, char *, char *);
void ReportError (WORD, char *);

__asm LONG PixLength (register __a0 char *, register __d1 LONG);
__asm LONG LabelPixLength (register __a0 char *);
__asm void SetNewGad (register __d0 ULONG, register __d1 ULONG);

BOOL OpenControlPanel (void);
void CloseControlPanel (void);
void CloseWindowSafely (struct Window *);

void GadgetEnable (ULONG);
void GadgetDisable (ULONG);
void GadgetActivate (ULONG);

void SetCheckbox (ULONG, UWORD);
void SetCycle (ULONG, UWORD);
void SetText (ULONG, char *);
void SetNumber (ULONG, LONG);
void SetString (ULONG, char *);
void SetSlider (ULONG, LONG);
void SetRadio (ULONG, UBYTE);

__asm void CalcCheckbox (register __d0 ULONG);
__asm void CalcCycle (register __d0 ULONG, register __a0 char **);
__asm void CalcText (register __d0 ULONG, register __a0 char *);
__asm void CalcLabel (register __d0 ULONG, register __a0 char *);
__asm void CalcBlank (register __d0 ULONG, register __a0 char *);
__asm void CalcButton (register __d0 ULONG, register __a0 char *);
__asm void CalcString (register __d0 ULONG, register __a0 char *);
__asm void CalcHSlider (register __d0 ULONG, register __a0 char *);
__asm void CalcVRadio (register __d0 ULONG, register __a0 char **);
__asm void CalcNumber (register __d0 ULONG, register __d1 LONG);
__asm void CalcList (register __d0 ULONG, register __d1 LONG, 
									register __a0 struct List *);

void BeginWindow (UBYTE, STRPTR);
void EndWindow (void);
void PushColumn (void);
void PopColumn (void);
struct EXT_GADGET *GetColumnInfo (void);
void GadgetAdd (ULONG);
void AlignToRight (ULONG);
void CalcPanelSize (void);

void CreateCheckbox (struct Gadget **, ULONG);
void CreateCycle (struct Gadget **, ULONG, STRPTR *);
void CreateString (struct Gadget **, ULONG, ULONG);
void CreateButton (struct Gadget **, ULONG);
void CreateLabel (struct Gadget **, ULONG);
void CreateText (struct Gadget **, ULONG);
void CreateNumber (struct Gadget **, ULONG);

/*
 * external variable refs
*/

extern struct EXT_GADGET	AllGads [GAD_MAXIMUM];
extern struct NewGadget		NGad;
extern struct PANEL			Panel;
extern struct Screen		*StdScreen;

#endif
