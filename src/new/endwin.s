
;--------------------------------------------------------------
; Entry:	<void>
; Exit:	Window setup finished.

_EndWindow:
	jsr	_CalcPanelSize
	jsr	_PopColumn
	rts
