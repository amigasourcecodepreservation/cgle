;--------------------------------------------------------------
; Entr:	d0=gadget ID
; Exit:	Gadget added to stack

_GadgetAdd:
	movem.l	d1/d7/a0-1,-(a7)

	move.l	_ExtGadPtr,d7		;a0=&ExtGadStack[ExtGadPtr-1]
	mulu	#XG_SIZEOF,d7
	lea	_ExtGadStack-XG_SIZEOF,a0
	add.l	d7,a0

	move.l	d0,d7			;a1=&AllGads[GadgetID]
	mulu	#XG_SIZEOF,d7
	lea	_AllGads,a1
	add.l	d7,a1

	move.b	_CurOrientation,d1		;Skip on orientation
	cmp.b	#HORIZONTAL,d1
	bne	.ga_curvert

	;Current orientation is horizontal

	;AllGads[Gadget].x = ExtGadStack[ptr-1].x 
	;			+ ExtGadStack[ptr-1].Width + INTERWIDTH;
	move.w	XG_x(a0),d7
	add.w	XG_Width(a0),d7
	add.w	#INTERWIDTH,d7
	move.w	d7,XG_x(a1)

	;AllGads [Gadget].y = ExtGadStack [ptr - 1].y + INTERHEIGHT;
	move.w	XG_y(a0),d7
	add.w	#INTERHEIGHT,d7
	move.w	d7,XG_y(a1)

	;ExtGadStack [ptr-1].Width += AllGads [Gadget].Width + INTERWIDTH;
	move.w	XG_Width(a1),d7
	add.w	#INTERWIDTH,d7
	add.w	d7,XG_Width(a0)

	;ExtGadStack [ptr-1].Height = 
	;		MAX (ExtGadStack [ptr-1].Height, 
	;			AllGads [Gadget].Height + INTERHEIGHT);
	move.w	XG_Height(a1),d7
	add.w	#INTERHEIGHT,d7
	cmp.w	XG_Height(a0),d7
	ble.s	.ga_done
	move.w	d7,XG_Height(a0)
	bra.s	.ga_done

.pc_curvert:
	;Current orientation is horizontal

	;AllGads [Gadget].x = ExtGadStack [ptr-1].x + INTERWIDTH;
	move.w	XG_x(a0),d7
	add.w	#INTERWIDTH,d7
	move.w	d7,XG_x(a1)

	;AllGads [Gadget].y = 
	;  ExtGadStack [ptr-1].y + ExtGadStack [ptr-1].Height + INTERHEIGHT;
	move.w	XG_y(a0),d7
	add.w	XG_Height(a0),d7
	add.w	#INTERHEIGHT,d7
	move.w	d7,XG_y(a1)

	;ExtGadStack [ptr-1].Height += AllGads [Gadget].Height + INTERHEIGHT;
	move.w	XG_Height(a1),d7
	add.w	#INTERHEIGHT,d7
	add.w	d7,XG_Height(a0)

	;ExtGadStack [ptr-1].Width = 
	;	MAX (ExtGadStack [ptr-1].Width, 
	;		AllGads [Gadget].Width + INTERWIDTH);

	move.w	XG_Width(a1),d7
	add.w	#INTERWIDTH,d7
	cmp.w	XG_Width(a0),d7
	ble.s	.ga_done
	move.w	d7,XG_Width(a0)

.ga_done: movem.l	(a7)+,d1/d7/a0-1
	rts
