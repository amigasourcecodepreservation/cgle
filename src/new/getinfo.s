
;--------------------------------------------------------------
; Entr:	<void>
; Exit:	d0=pointer to current gadget stack entry

_GetColumnInfo:
	move.l	_ExtGadPtr,d0
	mulu	#XG_SIZEOF,d0
	add.l	#_ExtGadStack,d0
	rts
