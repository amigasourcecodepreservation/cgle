;--------------------------------------------------------------
; Entr:	<void>
; Exit:	New column started

_PushColumn:
	movem.l	d0-1/d7/a0-1,-(a7)

	move.l	_ExtGadPtr,d0		;a0=&ExtGadStack[ExtGadPtr]
	move.l	d0,d7
	mulu	#XG_SIZEOF,d7
	lea	_ExtGadStack,a0
	add.l	d7,a0
	lea	-XG_SIZEOF(a0),a1		;a1=&ExtGadStack[ExtGadPtr-1]

	;Initialise new column dimensions
	moveq	#0,d7
	move.b	_LBWidth,d7		;ExtGadStack[].x = LBWidth;
	move.w	d7,XG_x(a0)
	move.b	_TBWidth,d7		;ExtGadStack[].y = TBWidth;
	move.w	d7,XG_y(a0)
	clr.w	XG_Width(a0)
	clr.w	XG_Height(a0)

	tst.l	d0			;Skip setup if first col
	ble	.pc_firstcol

	move.b	_CurOrientation,d1		;Currently horizontal?
	cmp.b	#HORIZONTAL,d1
	bne.s	.pc_curvert		;Skip if not

	;Current column is a horizontal

	move.w	XG_x(a1),d7		;col[].x = col[-1].x + col[-1].w
	add.w	XG_Width(a1),d7
	move.w	d7,XG_x(a0)

	move.w	XG_y(a1),XG_y(a0)		;col[].y = col[-1].y
	bra.s	.pc_firstcol

.pc_curvert:
	;Current column is a vertical

	move.w	XG_x(a1),XG_x(a0)		;col[].x = col[-1].x

	move.w	XG_y(a1),d7		;col[].y = col[-1].y + col[-1].h
	add.w	XG_Height(a1),d7
	move.w	d7,XG_y(a0)

.pc_firstcol:
	addq.l	#1,d0			;Increase stack pointer
	move.l	d0,_ExtGadPtr

	not.b	d1			;Flip orientation
	move.b	d1,_CurOrientation

	movem.l	(a7)+,d0-1/d7/a0-1
	rts
