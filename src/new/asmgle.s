;
; asmgle.s main file
;

	SECTION	gleasm,CODE

	INCDIR	INCLUDE:
	INCLUDE	graphics/graphics_lib.i
	INCLUDE	graphics/graphics_lib.i
	INCLUDE	intuition/intuition_lib.i
	INCLUDE 	intuition/screens.i
	INCLUDE	libraries/gadtools.i
	INCLUDE	libraries/gadtools_lib.i

	INCLUDE	"asmgle.i"

	;
	; Exported functions
	;

	XDEF	_WindowConstructor

	XDEF	_PixLength
	XDEF	_LabelPixLength
	XDEF	_SetNewGad

	XDEF	_CalcCheckbox
	XDEF	_CalcText
	XDEF	_CalcButton
	XDEF	_CalcLabel
	XDEF	_CalcBlank
	XDEF	_CalcCycle
	XDEF	_CalcString
	XDEF	_CalcHSlider
	XDEF	_CalcVRadio
	XDEF	_CalcNumber

	;
	; Imported variables/functions
	;

	XREF	init_xgad
	XREF	calc_xlength

	XREF.l	_GfxBase		;Library bases
	XREF.l	_IntuitionBase
	XREF.l	_GadToolsBase

	XREF.l	_AllGads		;struct EXT_GADGET AllGads[GAD_MAXIMUM]
	XREF.l	_NGad		;struct NewGadget

	XREF.l	_StdScreen	;struct Screen *
	XREF.l	_VisualInfo	;APTR
	XREF.l	_FontY		;WORD
	XREF.l	_TBWidth		;BYTE
	XREF.l	_BBWidth		;BYTE
	XREF.l	_LBWidth		;BYTE
	XREF.l	_RBWidth		;BYTE

;	XREF.l	_Panel		;struct PANEL

;--------------------------------------------------------------
mystrlen:
; Entry:	a0=pointer to text
; Exit:	d0=strlen(a0)

	move.l	a0,-(a7)

	moveq	#-1,d0
.mst_loop:
	addq.l	#1,d0
	tst.b	(a0)+
	bne.s	.mst_loop

	move.l	(a7)+,a0
	rts

;--------------------------------------------------------------
; Entry:	a0=pointer to array of string pointers
; Exit:	d0=number of strings in array
;	d1=PixLength of widest string in array

measure_strarray:
	movem.l	d2/a0-1,-(a7)

	move.l	a0,a1			;a1=pointer array
	moveq	#0,d1			;d1=widest string so far
	moveq	#0,d2			;d2=number of strings
.ms_widthloop:
	move.l	(a1)+,d0			;d0=ptr to next string
	beq.s	.ms_gotwidth

	addq.l	#1,d2			;Inc count of strings

	move.l	d0,a0			;Get width of current string
	bsr	_PixLength
	move.l	d0,d1			;d1=MAX(width, widestsofar)
	bra.s	.ms_widthloop

.ms_gotwidth:
	move.l	d2,d0			;d0=string count

	movem.l	(a7)+,d2/a0-1
	rts

;--------------------------------------------------------------
; Entry:	<void>
; Exit:	This function sets up all necessary system variables
;	for screen and window use.

_WindowConstructor:
	movem.l	d1/a0-1/a6,-(a7)

	move.l	_IntuitionBase,a6		;Get front screen handle
	sub.l	a0,a0
	jsr	_LVOLockPubScreen(a6)
	move.l	d0,_StdScreen
	beq.s	.wc_failed

	move.l	d0,a0			;a0=StdScreen
	move.w	sc_RastPort+rp_TxHeight(a0),d0 ;d0=FontY
	move.w	d0,_FontY
	move.b	sc_WBorLeft(a0),_LBWidth
	move.b	sc_WBorRight(a0),_RBWidth
	move.b	sc_WBorBottom(a0),_BBWidth
	add.b	sc_WBorTop(a0),d0		;Set TBWidth = WBorTop + FontY + 1
	addq.b	#1,d0
	move.b	d0,_TBWidth

	move.l	_GadToolsBase,a6		;Get VisualInfo for screen
	sub.l	a1,a1
	jsr	_LVOGetVisualInfoA(a6)
	move.l	d0,_VisualInfo
	beq.s	.wc_failed
	
.wc_ok:	movem.l	(a7)+,d1/a0-1/a6
	moveq	#-1,d0
	rts

.wc_failed:
	movem.l	(a7)+,d1/a0-1/a6
	moveq	#0,d0
	rts

;--------------------------------------------------------------
; This function returns the length in pixels for a given string
;
; Entry:	a0=pointer to text
;	d1=old length
; Exit:	d0=MAX(length(a0), old length)

_PixLength:
	movem.l	d2-7/a0-6,-(a7)
	move.l	d1,-(a7)

	bsr	mystrlen			;d0=strlen(a0)
	move.l	_StdScreen,a1
	lea	sc_RastPort(a1),a1		;a1=Screen's rastport

	move.l	_GfxBase,a6		;Get pixel length of string
	jsr	_LVOTextLength(a6)

	move.l	(a7)+,d1			;d1=old pixel length

	cmp.w	d0,d1			;Get MAX(d0,d1) in d0
	ble.s	.pl_gotmax
	move.w	d1,d0
.pl_gotmax:
	ext.l	d0

	movem.l	(a7)+,d2-7/a0-6
	rts

;--------------------------------------------------------------
; This function returns the length in pixels for a given label,
; ignoring "_" character(s)
;
; Entry:	a0=pointer to text
; Exit:	d0=MAX(length(a0), old length)

_LabelPixLength:
	movem.l	d1-7/a0-6,-(a7)
	lea	-110(a7),a7		;Create local variable space

	;Copy input string to local buffer, ignoring "_"

	move.l	a7,a1			;a1=local buffer
	lea	100(a7),a2		;a2=end of buffer
.lp_copyloop:
	move.b	(a0)+,d0
	beq.s	.lp_copydone
	cmp.b	#"_",d0
	beq.s	.lp_copyloop

	cmp.l	a1,a2			;Past end of local buffer?
	ble.s	.lp_bufferr
	move.b	d0,(a1)+
	bra.s	.lp_copyloop

.lp_bufferr:	;Internal buffer overflow
;ReportError (ERROR, "LabelPixLength: internal buffer overflow.");

.lp_copydone:
	clr.b	(a1)			;Terminate label string

	move.l	a7,a0			;Call PixLength on label
	moveq	#0,d1
	bsr.s	_PixLength

	lea	110(a7),a7		;Destroy local variable space
	movem.l	(a7)+,d1-7/a0-6
	rts

;--------------------------------------------------------------
; Entry:	d0=gadget index
;	d1=gadget flags
; Exit:	NewGadget structure initialised with values from
;	_AllGads array for this gadget.

_SetNewGad:
	movem.l	d0-1/a0-2,-(a7)

	lea	_NGad,a0			;a0=global NewGadget structure

	move.w	d0,gng_GadgetID(a0)		;Store gadget ID

	mulu	#XG_SIZEOF,d0		;Get a1=gadget address
	lea	_AllGads,a1
	add.l	d0,a1

	move.w	XG_x(a1),gng_LeftEdge(a0)	;Copy gadget boundaries
	move.w	XG_y(a1),gng_TopEdge(a0)
	move.w	XG_Width(a1),gng_Width(a0)
	move.w	XG_Height(a1),gng_Height(a0)

	move.l	XG_Label(a1),gng_GadgetText(a0)

	move.l	_StdScreen,a2		;Screen font info
	move.l	sc_Font(a2),gng_TextAttr(a0)

	move.l	d1,gng_Flags(a0)

	move.l	_VisualInfo,gng_VisualInfo(a0)

	clr.l	gng_UserData(a0)

	movem.l	(a7)+,d0-1/a0-2
	rts

;--------------------------------------------------------------
; Entry:	d0=gadget index
; Exit:	Checkbox gadget calculated

_CalcCheckbox:
	movem.l	d0-1/a0-1,-(a7)

	jsr	init_xgad			;Init x,y,height

	moveq	#3,d0			;Set width to "xxx"
	jsr	calc_xlength
	move.w	d0,XG_Width(a1)
	
	movem.l	(a7)+,d0-1/a0-1
	rts

;--------------------------------------------------------------
; Entry:	d0=gadget index
;	a0=text for gadget
; Exit:	Text gadget calculated

_CalcText:
_CalcString:
	movem.l	d0-1/a0-1,-(a7)

	jsr	init_xgad		;Init x,y,height

	;Width = PixLength (Text) + PixLength ("xx")
	
	moveq	#0,d1
	bsr	_PixLength
	move.w	d0,XG_Width(a1)

	moveq	#2,d0
	jsr	calc_xlength
	add.w	d0,XG_Width(a1)

	movem.l	(a7)+,d0-1/a0-1
	rts

;--------------------------------------------------------------
; Entry:	d0=gadget index
;	a0=text for gadget
; Exit:	HSlider gadget calculated

_CalcHSlider:
	movem.l	d0-1/a0-1,-(a7)

	jsr	init_xgad		;Init x,y,height

	;Width = PixLength (Text)
	
	moveq	#0,d1
	bsr	_PixLength
	move.w	d0,XG_Width(a1)

	movem.l	(a7)+,d0-1/a0-1
	rts

;--------------------------------------------------------------
; Entry:	d0=gadget index
;	a0=text for gadget
; Exit:	Button gadget calculated

_CalcButton:
	movem.l	d0-1/a0-1,-(a7)

	jsr	init_xgad		;Init x,y,height

	move.l	a0,XG_Label(a1)

	;Width = LabelPixLength (Label) + PixLength ("xx", 0);

	moveq	#0,d1
	bsr	_LabelPixLength
	move.w	d0,XG_Width(a1)

	moveq	#2,d0
	jsr	calc_xlength
	add.w	d0,XG_Width(a1)

	movem.l	(a7)+,d0-1/a0-1
	rts

;--------------------------------------------------------------
; Entry:	d0=gadget index
;	a0=text for gadget
; Exit:	Label gadget calculated

_CalcLabel:
	movem.l	d0-1/a0-1,-(a7)

	jsr	init_xgad			;Init x,y,height

	move.l	a0,XG_Label(a1)

	moveq	#0,d1			;Width=LabelPixLength(Label)
	bsr	_LabelPixLength
	move.w	d0,XG_Width(a1)

	movem.l	(a7)+,d0-1/a0-1
	rts

;--------------------------------------------------------------
; Entry:	d0=gadget index
;	a0=text for gadget
; Exit:	Blank gadget calculated

_CalcBlank:
	movem.l	d0-1/a0-1,-(a7)

	jsr	init_xgad			;Init x,y,height
	clr.l	XG_Label(a1)

	moveq	#0,d1			;Width=LabelPixLength(Label)
	bsr	_LabelPixLength
	move.w	d0,XG_Width(a1)

	movem.l	(a7)+,d0-1/a0-1
	rts

;--------------------------------------------------------------
; Entry:	d0=gadget index
;	a0=pointer to array of pointers to cycle values
; Exit:	Cycle gadget calculated

_CalcCycle:
	movem.l	d0-1/a0-1,-(a7)

	jsr	init_xgad		;Init x,y,height

	;Set width to width of longest cycle string + "xxxx"

	bsr	measure_strarray		;d1=width of widest gadget text
	moveq	#4,d0			;Add "xxxx" width to widest
	jsr	calc_xlength
	add.w	d1,d0

	move.w	d0,XG_Width(a1)

	movem.l	(a7)+,d0-1/a0-1
	rts

;--------------------------------------------------------------
; Entry:	d0=gadget index
;	a0=pointer to array of gadget text pointers
; Exit:	VRadio gadget calculated

_CalcVRadio:
	movem.l	d0-2/a0-2,-(a7)

	jsr	init_xgad			;Init x,y,height
	clr.l	XG_Label(a1)

	bsr	measure_strarray		;d1=width of widest gadget text
	move.l	d0,d2			;d2=number of strings

	moveq	#3,d0			;Add "xxx" width of widest
	jsr	calc_xlength
	add.l	d1,d0

	move.w	d0,XG_Width(a1)

	;Set height = (FontY + 4 + INTERHEIGHT) * numstrings - INTERHEIGHT;
	move.w	XG_Height(a1),d0		;d0=FontY
	add.w	#4+INTERHEIGHT,d0		;d0=FontY + 4 + INTERHEIGHT
	mulu	d2,d0
	sub.w	#INTERHEIGHT,d0
	move.w	d0,XG_Height(a1)

	movem.l	(a7)+,d0-2/a0-2
	rts

;--------------------------------------------------------------
; Entry:	d0=gadget index
;	d1=value for number gadget
; Exit:	Number gadget calculated

_CalcNumber:
	movem.l	d0-1/a0-1,-(a7)
	sub.l	#20,a7

	jsr	init_xgad			;Init x,y,height

	;Build a string of "8"'s as long as the number of
	;digits in the number in d1.
	move.l	a7,a0

	tst.l	d1			;<= 0 ?
	bpl.s	.cn_loop			;Skip if >0

	move.b	#"-",(a0)+		;Insert "-" to string
	neg.l	d1

.cn_loop:	move.b	#"8",(a0)+
	divu	#10,d1
	ext.l	d1

	tst.l	d1			;Any digits left?
	bne.s	.cn_loop			;Loop back if yes
	clr.b	(a0)

	moveq	#0,d1			;Width=PixLength(number)
	move.l	a7,a0
	bsr	_PixLength
	move.w	d0,XG_Width(a1)

	add.l	#20,a7
	movem.l	(a7)+,d0-1/a0-1
	rts

;--------------------------------------------------------------
; Entry:	d0=gadget index
;	d1=number of visible lines
;	a0=pointer to list of items
; Exit:	List gadget calculated

_CalcList:
	movem.l	d0-1/a0-2,-(a7)

	jsr	init_xgad			;Init x,y,height

	clr.l	XG_Label(a1)

	;Set height = (FontY + 4 + INTERHEIGHT) * Lines - INTERHEIGHT;
	move.w	XG_Height(a1),d0		;d0=FontY
	add.w	#4+INTERHEIGHT,d0		;d0=FontY + 4 + INTERHEIGHT
	mulu	d1,d0
	sub.w	#INTERHEIGHT,d0
	move.w	d0,XG_Height(a1)

	;Get width of widest item string
	moveq	#0,d1			;d1=width
	move.l	lh_Head(a0),d0		;d0=first node in item list
	beq.s	.cl_gotwidth		;Handle empty list
.cl_widthloop:
	move.l	d0,a2
	move.l	ln_Name(a2),a0		;Get width of current string
	bsr	_PixLength
	move.l	d0,d1			;d1=MAX(width, widestsofar)

	move.l	ln_Succ(a2),d0		;d0=next node
	bne.s	.cl_widthloop

.cl_gotwidth:
	moveq	#4,d0			;Add "xxxx" width to widest
	jsr	calc_xlength
	add.w	d1,d0

	move.w	d0,XG_Width(a1)

	movem.l	(a7)+,d0-1/a0-2
	rts
