;--------------------------------------------------------------
; Entry:	d0 = orientation to start with
;	a0 = window title
; Exit:	Window setup begun.

_BeginWindow:
	movem.l	d0-2/a0-1,-(a7)

	move.b	d0,_CurOrientation		;Set orientation

	lea	_Panel,a1			;Set title
	move.l	a0,PAN_Title(a1)

	;Set window width to PixLength(Title) + 9 * PixLength("X")
	;This is the minimum value and might increase.

	moveq	#0,d1
	jsr	_PixLength
	move.l	d0,d2

	moveq	#9,d0
	jsr	calc_xlength
	add.w	d0,d2
	move.l	d2,PAN_Width(a1)

	move.l	d2,_MaxWidth		;Set MaxWidth to window width

	jsr	_PushColumn

	movem.l	(a7)+,d0-2/a0-1
	rts
