;--------------------------------------------------------------
; Entr:	<void>
; Exit:	Current column finished off

_PopColumn:
	movem.l	d0-1/d7/a0-1,-(a7)

	move.b	_CurOrientation,d1		;Flip orientation
	not.b	d1
	move.b	d1,_CurOrientation,d1

	move.l	_ExtGadPtr,d0		;Pop from stack
	subq.l	#1,d0
	move.l	d0,_ExtGadPtr

	ble.s	.pc_firstcol		;Skip if first column

	move.l	d0,d7			;a0=&ExtGadStack[ExtGadPtr]
	mulu	#XG_SIZEOF,d7
	lea	_ExtGadStack,a0
	add.l	d7,a0
	lea	-XG_SIZEOF(a0),a1		;a1=&ExtGadStack[ExtGadPtr-1]

	cmp.b	#HORIZONTAL,d1		;Horiz or vertical now?
	bne.s	.pc_curvert		;Skip if vertical

	;Current orientation is horizontal
	add.w	XG_Width(a0),XG_Width(a1)	;col[-1].w += col[].w

	move.w	XG_Height(a0),d7		;col[-1].h = MAX(col[-1].h, col[].h)
	cmp.w	XG_Height(a1),d7
	ble.s	.pc_setmaxwidth
	move.w	d7,XG_Height(a1)
	bra.s	.pc_setmaxwidth

.pc_curvert:
	;Current orientation is vertical
	add.w	XG_Height(a0),XG_Height(a1)	;col[-1].h += col[].h

	move.w	XG_Width(a0),d7		;col[-1].w = MAX(col[-1].w, col[].w)
	cmp.w	XG_Width(a1),d7
	ble.s	.pc_setmaxwidth
	move.w	d7,XG_Width(a1)

.pc_setmaxwidth:
	move.l	XG_Width(a1),d7		;If current col is wider
	cmp.l	_MaxWidth,d7		;than max, set new max.
	ble.s	.pc_gotmaxwidth
	move.l	d7,_MaxWidth

.pc_gotmaxwidth:
.pc_firstcol:

	movem.l	(a7)+,d0-1/d7/a0-1
	rts
