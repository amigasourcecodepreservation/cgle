
	SECTION	measurestr,CODE

	INCDIR	INCLUDE:
	INCLUDE	exec/lists.i
	INCLUDE	libraries/gadtools.i
	INCLUDE	"asmgle.i"

	;
	; Exported functions
	;

	XDEF	_CalcList

	;
	; Imported variables/functions
	;

	XREF	init_xgad
	XREF	calc_xlength
	XREF	_PixLength

;--------------------------------------------------------------
; Entry:	d0=gadget index
;	d1=number of visible lines
;	a0=pointer to list of items
; Exit:	List gadget calculated

_CalcList:
	movem.l	d0-1/a0-2,-(a7)

	jsr	init_xgad			;Init x,y,height

	clr.l	XG_Label(a1)

	;Set height = (FontY + 4 + INTERHEIGHT) * Lines - INTERHEIGHT;
	move.w	XG_Height(a1),d0		;d0=FontY
	add.w	#4+INTERHEIGHT,d0		;d0=FontY + 4 + INTERHEIGHT
	mulu	d1,d0
	sub.w	#INTERHEIGHT,d0
	move.w	d0,XG_Height(a1)

	;Get width of widest item string
	moveq	#0,d1			;d1=width
	move.l	LH_HEAD(a0),d0		;d0=first node in item list
	beq.s	.cl_gotwidth		;Handle empty list
.cl_widthloop:
	move.l	d0,a2
	move.l	LN_NAME(a2),a0		;Get width of current string
	bsr	_PixLength
	move.l	d0,d1			;d1=MAX(width, widestsofar)

	move.l	LN_SUCC(a2),d0		;d0=next node
	bne.s	.cl_widthloop

.cl_gotwidth:
	moveq	#4,d0			;Add "xxxx" width to widest
	jsr	calc_xlength
	add.w	d1,d0

	move.w	d0,XG_Width(a1)

	movem.l	(a7)+,d0-1/a0-2
	rts
