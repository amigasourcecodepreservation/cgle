
	SECTION	initxgad,CODE

	INCLUDE	"asmgle.i"

	;
	; Exported functions
	;

	XDEF	init_xgad


	;
	; Imported variables/functions
	;

	XREF.l	_AllGads		;struct EXT_GADGET AllGads[GAD_MAXIMUM]
	XREF.l	_FontY		;WORD

;--------------------------------------------------------------
; Entry:	d0=gadget index
; Exit:	a1=gadget structure address
;	Gadget initialised:
;	x,y    = 0
;	Height = FontY+4

init_xgad:
	movem.l	d0/a0,-(a7)

	mulu	#XG_SIZEOF,d0	;Get a1=gadget address
	lea	_AllGads,a1
	add.l	d0,a1

	moveq	#0,d0
	move.w	d0,XG_x(a1)
	move.w	d0,XG_y(a1)

	move.w	_FontY,d0
	addq.w	#4,d0
	move.w	d0,XG_Height(a1)

	movem.l	(a7)+,d0/a0
	rts
