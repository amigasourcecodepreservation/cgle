
	SECTION	calchslider,CODE

	INCLUDE	"asmgle.i"

	;
	; Exported functions
	;

	XDEF	_CalcHSlider

	;
	; Imported variables/functions
	;

	XREF	init_xgad
	XREF	_PixLength

;--------------------------------------------------------------
; Entry:	d0=gadget index
;	a0=text for gadget
; Exit:	HSlider gadget calculated

_CalcHSlider:
	movem.l	d0-1/a0-1,-(a7)

	jsr	init_xgad		;Init x,y,height

	;Width = PixLength (Text)
	
	moveq	#0,d1
	bsr	_PixLength
	move.w	d0,XG_Width(a1)

	movem.l	(a7)+,d0-1/a0-1
	rts
