/*
 * gle.c main file
*/

#define INTUI_V36_NAMES_ONLY

#include <clib/alib_protos.h>
#include <clib/asl_protos.h>
#include <clib/dos_protos.h>
#include <clib/exec_protos.h>
#include <clib/gadtools_protos.h>
#include <clib/graphics_protos.h>
#include <clib/icon_protos.h>
#include <clib/graphics_protos.h>
#include <clib/intuition_protos.h>
#include <clib/wb_protos.h>

#include <pragmas/asl_pragmas.h>
#include <pragmas/dos_pragmas.h>
#include <pragmas/exec_pragmas.h>
#include <pragmas/gadtools_pragmas.h>
#include <pragmas/graphics_pragmas.h>
#include <pragmas/icon_pragmas.h>
#include <pragmas/graphics_pragmas.h>
#include <pragmas/intuition_pragmas.h>
#include <pragmas/wb_pragmas.h>

#include <dos.h>
#include <exec/lists.h>
#include <exec/memory.h>
#include <exec/nodes.h>
#include <exec/types.h>
#include <intuition/gadgetclass.h>
#include <stdio.h>
#include <string.h>

#include "gle.h"

static char Version[] = "$VER: Gadtools Layout Library v1.01 (" __DATE__ " " __TIME__ ")";

/***** GLOBAL VARIABLES *****/

struct	EXT_GADGET AllGads [GAD_MAXIMUM] = {NULL};
struct	NewGadget NGad = {0};
struct	PANEL Panel = {0};
struct	Screen *StdScreen = NULL;
APTR	VisualInfo = NULL;

BYTE	LBWidth=0;
BYTE	RBWidth=0;
BYTE	TBWidth=0;
BYTE	BBWidth=0;
WORD	FontY=0;

// For the window layout engine

struct	EXT_GADGET ExtGadStack [20] = {NULL};
LONG	ExtGadPtr = 0;
UBYTE	CurOrientation = 0;
LONG	MaxWidth = 0;

/***** EXTERNAL VARIABLES *****/

extern struct Library *IntuitionBase;
extern struct Library *GadToolsBase;
extern struct Library *GfxBase;


/*	This function frees all screen-related system structures.
*/

void WindowDestructor (void)
{
	/* Close our window and free the gadgets
	*/
	CloseControlPanel();

	if (Panel.GList)
	{
		FreeGadgets (Panel.GList);
		Panel.GList = NULL;
	}

	if (VisualInfo)
	{
		FreeVisualInfo (VisualInfo);
		VisualInfo = NULL;
	}

	if (StdScreen)
	{
		UnlockPubScreen (NULL, StdScreen);
		StdScreen = NULL;
	}
}


/***** REQUESTERS *****/

/*	This function asks the user a question.

	in: Title: the title of the requester.
	in: Question: the messagebody shown in the requester.
	in: Answers: the various possible responses.

	out: the selected response. Similar to EasyRequest ().
*/

LONG RequestUserInput (char *Title, char *Message, char *Responses)
{	LONG Result;
	struct EasyStruct MessageStruct;

	MessageStruct.es_StructSize = sizeof (struct EasyStruct);
	MessageStruct.es_Flags = 0;
	MessageStruct.es_Title = Title;
	MessageStruct.es_TextFormat = Message;
	MessageStruct.es_GadgetFormat = Responses;

	Result = EasyRequest (Panel.Win, &MessageStruct, 0);

	return Result;
}



/*	This function reports an error.
*/

void ReportError (WORD Level, char *Message)
{	switch (Level) {
		case WARN:
			RequestUserInput ("Warning", Message, "Okay");
			break;
		case ERROR:
			RequestUserInput ("Error", Message, "Okay");
			break;
		case BUG:
			RequestUserInput ("Bug", Message, "Okay");
			break;
	}
}


/*	This function opens the control basic panels
*/

BOOL OpenControlPanel (void)
{	WORD ZoomSize [4];

	/* If possible, bring an old window to front */

	if (Panel.Win)
	{
		WindowToFront (Panel.Win);
		ActivateWindow (Panel.Win);
		return TRUE;
	}

	/* Open a new window */
	ZoomSize [0] = 0;
	ZoomSize [1] = 0;
	ZoomSize [2] = 5 * PixLength ("XXXXX", 0);
	ZoomSize [3] = TBWidth;

	Panel.Width = MIN(Panel.Width, StdScreen->Width);
	Panel.Height= MIN(Panel.Height, StdScreen->Height);

	Panel.WinX	= ((StdScreen->Width - Panel.Width) / 2);
	Panel.WinY	= ((StdScreen->Height - Panel.Height) / 2);

	Panel.Win = OpenWindowTags (NULL,
					WA_Left, Panel.WinX,
					WA_Top, Panel.WinY,
					WA_Width, Panel.Width,
					WA_Height, Panel.Height,
					WA_CloseGadget, TRUE,
					WA_DepthGadget, TRUE,
					WA_DragBar, TRUE,
					WA_Gadgets, Panel.GList,
					WA_Zoom, ZoomSize,
					WA_PubScreen, StdScreen,
					WA_Activate, TRUE,
					WA_SimpleRefresh, TRUE,
					WA_NewLookMenus, TRUE,
					WA_IDCMP, 0L,
					WA_RMBTrap, TRUE,
					WA_Title, Panel.Title,
					TAG_DONE);

	if (!Panel.Win) return FALSE;

	Panel.Win->UserData = (void *)(&Panel);

	ModifyIDCMP (Panel.Win, IDCMP_CLOSEWINDOW |
							SLIDERIDCMP |
							MXIDCMP |
							BUTTONIDCMP |
							STRINGIDCMP |
							CYCLEIDCMP |
							IDCMP_MENUPICK |
							IDCMP_MOUSEMOVE |
							IDCMP_VANILLAKEY |
							IDCMP_CHANGEWINDOW |
							IDCMP_REFRESHWINDOW);

	GT_RefreshWindow (Panel.Win, NULL);

	return TRUE;
}




/*	This function removes the control panel from memory.
*/

void CloseControlPanel (void)
{
	if (Panel.Win)
	{
		ClearMenuStrip (Panel.Win);
		CloseWindowSafely (Panel.Win);
		Panel.Win = NULL;
	}
}




/*	This function closes a window with a shared IDCMP. Taken from
	Amiga RKM 3rd edition.
*/

void CloseWindowSafely (struct Window *Win)
{	struct IntuiMessage *msg;
	struct Node *succ;

	Forbid ();

	/* remove all messages for our window */

	msg = (struct IntuiMessage *)Win->UserPort->mp_MsgList.lh_Head;

	while (succ = msg->ExecMessage.mn_Node.ln_Succ) {
		if (msg->IDCMPWindow == Win) {
			Remove ((struct Node *)msg);
			ReplyMsg ((struct Message *)msg);
		}
		msg = (struct IntuiMessage *)succ;
	}

	Win->UserPort = NULL;

	ModifyIDCMP (Win, 0L);

	Permit ();

	CloseWindow (Win);
}




/*	The following functions perform operations on gadgets.
*/




void GadgetEnable (ULONG Gadget)
{	GT_SetGadgetAttrs (AllGads [Gadget].Gad, Panel.Win, NULL,
		GA_Disabled, FALSE,
		TAG_DONE);

	AllGads [Gadget].Disabled = 0;
}

void GadgetDisable (ULONG Gadget)
{	GT_SetGadgetAttrs (AllGads [Gadget].Gad, Panel.Win, NULL,
		GA_Disabled, TRUE,
		TAG_DONE);

	AllGads [Gadget].Disabled = 1;
}

void GadgetActivate (ULONG Gadget)
{	if (!AllGads [Gadget].Disabled)
		ActivateGadget (AllGads [Gadget].Gad, Panel.Win, NULL);
}




/*	This function sets a checkbox to a specified state.
*/

void SetCheckbox (ULONG Gadget, UWORD State)
{
	GT_SetGadgetAttrs (AllGads [Gadget].Gad, Panel.Win, NULL,
		GTCB_Checked, State,
		TAG_END);
}


/*	This function sets a cycle gadget to a specified state.
*/

void SetCycle (ULONG Gadget, UWORD State)
{	GT_SetGadgetAttrs (AllGads [Gadget].Gad, Panel.Win, NULL,
		GTCY_Active, State,
		TAG_END);
}




/*	This function sets a text gadget to a specified string.
*/

void SetText (ULONG Gadget, char *Text)
{	GT_SetGadgetAttrs (AllGads [Gadget].Gad, Panel.Win, NULL,
		GTTX_Text, Text,
		TAG_END);
}




/*	This function sets a number gadget to a specified value.
*/

void SetNumber (ULONG Gadget, LONG Value)
{	GT_SetGadgetAttrs (AllGads [Gadget].Gad, Panel.Win, NULL,
		GTNM_Number, Value,
		TAG_END);
}




/*	This function sets a string gadget to a specified string.
*/

void SetString (ULONG Gadget, char *Text)
{	GT_SetGadgetAttrs (AllGads [Gadget].Gad, Panel.Win, NULL,
		GTST_String, Text,
		TAG_END);
}




/*	This function sets a slider to a specified value.
*/

void SetSlider (ULONG Gadget, LONG Value)
{	GT_SetGadgetAttrs (AllGads [Gadget].Gad, Panel.Win, NULL,
		GTSL_Level, Value,
		TAG_END);
}




/*	This function sets a radiobutton to a specified value.
*/

void SetRadio (ULONG Gadget, UBYTE Value)
{	GT_SetGadgetAttrs (AllGads [Gadget].Gad, Panel.Win, NULL,
		GTMX_Active, Value,
		TAG_END);
}


void CreateCheckbox (struct Gadget **MyGadget, ULONG GadNumber)
{	SetNewGad (GadNumber, PLACETEXT_LEFT);

	*MyGadget = CreateGadget (CHECKBOX_KIND, *MyGadget, &NGad,
			GT_Underscore, '_',
			GTCB_Scaled, TRUE,
			TAG_DONE);
	AllGads [GadNumber].Gad = *MyGadget;
}

void CreateCycle (struct Gadget **MyGadget, ULONG GadNumber, STRPTR *Labels)
{	SetNewGad (GadNumber, PLACETEXT_LEFT);
	*MyGadget = CreateGadget (CYCLE_KIND, *MyGadget, &NGad,
			GT_Underscore, '_',
			GTCY_Labels, Labels,
			TAG_DONE);
	AllGads [GadNumber].Gad = *MyGadget;
}

void CreateString (struct Gadget **MyGadget, ULONG GadNumber, ULONG MaxChars)
{	SetNewGad (GadNumber, PLACETEXT_LEFT);
	*MyGadget = CreateGadget (STRING_KIND, *MyGadget, &NGad,
			GT_Underscore, '_',
			GTST_MaxChars, MaxChars,
			TAG_DONE);
	AllGads [GadNumber].Gad = *MyGadget;
}

void CreateButton (struct Gadget **MyGadget, ULONG GadNumber)
{	SetNewGad (GadNumber, PLACETEXT_IN);
	*MyGadget = CreateGadget (BUTTON_KIND, *MyGadget, &NGad,
			GT_Underscore, '_',
			TAG_END);
	AllGads [GadNumber].Gad = *MyGadget;
}

void CreateLabel (struct Gadget **MyGadget, ULONG GadNumber)
{	SetNewGad (GadNumber, PLACETEXT_IN);
	*MyGadget = CreateGadget (TEXT_KIND, *MyGadget, &NGad,
			GT_Underscore, '_',
			GTTX_Border, FALSE,
			TAG_END);
	AllGads [GadNumber].Gad = *MyGadget;
}

void CreateText (struct Gadget **MyGadget, ULONG GadNumber)
{	SetNewGad (GadNumber, PLACETEXT_IN);
	*MyGadget = CreateGadget (TEXT_KIND, *MyGadget, &NGad,
			GT_Underscore, '_',
			GTTX_Border, TRUE,
			TAG_END);
	AllGads [GadNumber].Gad = *MyGadget;
}

void CreateNumber (struct Gadget **MyGadget, ULONG GadNumber)
{	SetNewGad (GadNumber, PLACETEXT_IN);
	*MyGadget = CreateGadget (NUMBER_KIND, *MyGadget, &NGad,
			GT_Underscore, '_',
			GTNM_Border, TRUE,
			TAG_END);
	AllGads [GadNumber].Gad = *MyGadget;
}
