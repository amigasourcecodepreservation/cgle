
	SECTION	calcblank,CODE

	INCLUDE	"asmgle.i"

	;
	; Exported functions
	;

	XDEF	_CalcBlank

	;
	; Imported variables/functions
	;

	XREF	init_xgad
	XREF	_LabelPixLength

;--------------------------------------------------------------
; Entry:	d0=gadget index
;	a0=text for gadget
; Exit:	Blank gadget calculated

_CalcBlank:
	movem.l	d0-1/a0-1,-(a7)

	jsr	init_xgad			;Init x,y,height
	clr.l	XG_Label(a1)

	moveq	#0,d1			;Width=LabelPixLength(Label)
	bsr	_LabelPixLength
	move.w	d0,XG_Width(a1)

	movem.l	(a7)+,d0-1/a0-1
	rts

