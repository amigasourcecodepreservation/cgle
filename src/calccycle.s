
	SECTION	measurestr,CODE

	INCLUDE	"asmgle.i"

	;
	; Exported functions
	;

	XDEF	_CalcCycle

	;
	; Imported variables/functions
	;

	XREF	init_xgad
	XREF	calc_xlength
	XREF	measure_strarray
	XREF	_PixLength

;--------------------------------------------------------------
; Entry:	d0=gadget index
;	a0=pointer to array of pointers to cycle values
; Exit:	Cycle gadget calculated

_CalcCycle:
	movem.l	d0-1/a0-1,-(a7)

	jsr	init_xgad		;Init x,y,height

	;Set width to width of longest cycle string + "xxxx"

	bsr	measure_strarray		;d1=width of widest gadget text
	moveq	#4,d0			;Add "xxxx" width to widest
	jsr	calc_xlength
	add.w	d1,d0

	move.w	d0,XG_Width(a1)

	movem.l	(a7)+,d0-1/a0-1
	rts
