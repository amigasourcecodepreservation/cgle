
	SECTION	measurestr,CODE

	INCDIR	INCLUDE:
	INCLUDE	exec/lists.i
	INCLUDE	libraries/gadtools.i
	INCLUDE	"asmgle.i"

	;
	; Exported functions
	;

	XDEF	_CalcVRadio

	;
	; Imported variables/functions
	;

	XREF	init_xgad
	XREF	calc_xlength
	XREF	measure_strarray
	XREF	_PixLength

;--------------------------------------------------------------
; Entry:	d0=gadget index
;	a0=pointer to array of gadget text pointers
; Exit:	VRadio gadget calculated

_CalcVRadio:
	movem.l	d0-2/a0-2,-(a7)

	jsr	init_xgad			;Init x,y,height
	clr.l	XG_Label(a1)

	jsr	measure_strarray		;d1=width of widest gadget text
	move.l	d0,d2			;d2=number of strings

	moveq	#3,d0			;Add "xxx" width of widest
	jsr	calc_xlength
	add.l	d1,d0

	move.w	d0,XG_Width(a1)

	;Set height = (FontY + 4 + INTERHEIGHT) * numstrings - INTERHEIGHT;
	move.w	XG_Height(a1),d0		;d0=FontY
	add.w	#4+INTERHEIGHT,d0		;d0=FontY + 4 + INTERHEIGHT
	mulu	d2,d0
	sub.w	#INTERHEIGHT,d0
	move.w	d0,XG_Height(a1)

	movem.l	(a7)+,d0-2/a0-2
	rts
