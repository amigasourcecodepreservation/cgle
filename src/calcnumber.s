
	SECTION	measurestr,CODE

	INCLUDE	"asmgle.i"

	;
	; Exported functions
	;

	XDEF	_CalcNumber

	;
	; Imported variables/functions
	;

	XREF	init_xgad
	XREF	calc_xlength
	XREF	_PixLength

;--------------------------------------------------------------
; Entry:	d0=gadget index
;	d1=value for number gadget
; Exit:	Number gadget calculated

_CalcNumber:
	movem.l	d0-1/a0-1,-(a7)
	sub.l	#20,a7

	jsr	init_xgad			;Init x,y,height

	;Build a string of "8"'s as long as the number of
	;digits in the number in d1.
	move.l	a7,a0

	tst.l	d1			;<= 0 ?
	bpl.s	.cn_loop			;Skip if >0

	move.b	#"-",(a0)+		;Insert "-" to string
	neg.l	d1

.cn_loop:	move.b	#"8",(a0)+
	divu	#10,d1
	ext.l	d1

	tst.l	d1			;Any digits left?
	bne.s	.cn_loop			;Loop back if yes
	clr.b	(a0)

	moveq	#0,d1			;Width=PixLength(number)
	move.l	a7,a0
	bsr	_PixLength
	move.w	d0,XG_Width(a1)

	add.l	#20,a7
	movem.l	(a7)+,d0-1/a0-1
	rts
