
	SECTION	gleasm,CODE

	INCDIR	INCLUDE:
	INCLUDE	exec/lists.i

	INCLUDE	graphics/graphics_lib.i
	INCLUDE	intuition/intuition_lib.i
	INCLUDE intuition/screens.i
	INCLUDE	libraries/gadtools.i
	INCLUDE	libraries/gadtools_lib.i

	INCLUDE	"asmgle.i"

	;
	; Exported functions
	;

	XDEF	_WindowConstructor
	XDEF	_PixLength
	XDEF	_LabelPixLength
	XDEF	_SetNewGad

	XDEF	_BeginWindow
	XDEF	_EndWindow
	XDEF	_PushColumn
	XDEF	_PopColumn
	XDEF	_GetColumnInfo
	XDEF	_GadgetAdd
	XDEF	_CalcPanelSize
	XDEF	_AlignToRight

	;
	; Imported variables/functions
	;

	XREF	init_xgad
	XREF	calc_xlength
	XREF	measure_strarray

	XREF.l	_GfxBase		;Library bases
	XREF.l	_IntuitionBase
	XREF.l	_GadToolsBase

	XREF.l	_AllGads		;struct EXT_GADGET AllGads[GAD_MAXIMUM]
	XREF.l	_NGad		;struct NewGadget

	XREF.l	_StdScreen	;struct Screen *
	XREF.l	_VisualInfo	;APTR
	XREF.l	_FontY		;WORD
	XREF.l	_TBWidth		;BYTE
	XREF.l	_BBWidth		;BYTE
	XREF.l	_LBWidth		;BYTE
	XREF.l	_RBWidth		;BYTE

	XREF.l	_ExtGadPtr	;LONG
	XREF.l	_ExtGadStack	;struct EXT_GADGET ExtGadStack [20] 
	XREF.l	_MaxWidth		;ULONG
	XREF.l	_CurOrientation	;BYTE
	XREF.l	_Panel		;struct PANEL

;--------------------------------------------------------------
mystrlen:
; Entry:	a0=pointer to text
; Exit:	d0=strlen(a0)

	move.l	a0,-(a7)

	moveq	#-1,d0
.mst_loop:
	addq.l	#1,d0
	tst.b	(a0)+
	bne.s	.mst_loop

	move.l	(a7)+,a0
	rts

;--------------------------------------------------------------
; Entry:	<void>
; Exit:	This function sets up all necessary system variables
;	for screen and window use.

_WindowConstructor:
	movem.l	d1/a0-1/a6,-(a7)

	move.l	_IntuitionBase,a6		;Get front screen handle
	sub.l	a0,a0
	jsr	_LVOLockPubScreen(a6)
	move.l	d0,_StdScreen
	beq.s	.wc_failed

	move.l	d0,a0			;a0=StdScreen
	move.w	sc_RastPort+rp_TxHeight(a0),d0 ;d0=FontY
	move.w	d0,_FontY
	move.b	sc_WBorLeft(a0),_LBWidth
	move.b	sc_WBorRight(a0),_RBWidth
	move.b	sc_WBorBottom(a0),_BBWidth
	add.b	sc_WBorTop(a0),d0		;Set TBWidth = WBorTop + FontY + 1
	addq.b	#1,d0
	move.b	d0,_TBWidth

	move.l	_GadToolsBase,a6		;Get VisualInfo for screen
	sub.l	a1,a1
	jsr	_LVOGetVisualInfoA(a6)
	move.l	d0,_VisualInfo
	beq.s	.wc_failed
	
.wc_ok:	movem.l	(a7)+,d1/a0-1/a6
	moveq	#-1,d0
	rts

.wc_failed:
	movem.l	(a7)+,d1/a0-1/a6
	moveq	#0,d0
	rts

;--------------------------------------------------------------
; This function returns the length in pixels for a given string
;
; Entry:	a0=pointer to text
;	d1=old length
; Exit:	d0=MAX(length(a0), old length)

_PixLength:
	movem.l	d2-7/a0-6,-(a7)
	move.l	d1,-(a7)

	bsr	mystrlen			;d0=strlen(a0)
	move.l	_StdScreen,a1
	lea	sc_RastPort(a1),a1		;a1=Screen's rastport

	move.l	_GfxBase,a6		;Get pixel length of string
	jsr	_LVOTextLength(a6)

	move.l	(a7)+,d1			;d1=old pixel length

	cmp.w	d0,d1			;Get MAX(d0,d1) in d0
	ble.s	.pl_gotmax
	move.w	d1,d0
.pl_gotmax:
	ext.l	d0

	movem.l	(a7)+,d2-7/a0-6
	rts

;--------------------------------------------------------------
; This function returns the length in pixels for a given label,
; ignoring "_" character(s)
;
; Entry:	a0=pointer to text
; Exit:	d0=MAX(length(a0), old length)

_LabelPixLength:
	movem.l	d1-7/a0-6,-(a7)
	lea	-110(a7),a7		;Create local variable space

	;Copy input string to local buffer, ignoring "_"

	move.l	a7,a1			;a1=local buffer
	lea	100(a7),a2		;a2=end of buffer
.lp_copyloop:
	move.b	(a0)+,d0
	beq.s	.lp_copydone
	cmp.b	#"_",d0
	beq.s	.lp_copyloop

	cmp.l	a1,a2			;Past end of local buffer?
	ble.s	.lp_bufferr
	move.b	d0,(a1)+
	bra.s	.lp_copyloop

.lp_bufferr:	;Internal buffer overflow
;ReportError (ERROR, "LabelPixLength: internal buffer overflow.");

.lp_copydone:
	clr.b	(a1)			;Terminate label string

	move.l	a7,a0			;Call PixLength on label
	moveq	#0,d1
	bsr.s	_PixLength

	lea	110(a7),a7		;Destroy local variable space
	movem.l	(a7)+,d1-7/a0-6
	rts

;--------------------------------------------------------------
; Entry:	d0=gadget index
;	d1=gadget flags
; Exit:	NewGadget structure initialised with values from
;	_AllGads array for this gadget.

_SetNewGad:
	movem.l	d0-1/a0-2,-(a7)

	lea	_NGad,a0			;a0=global NewGadget structure

	move.w	d0,gng_GadgetID(a0)		;Store gadget ID

	mulu	#XG_SIZEOF,d0		;Get a1=gadget address
	lea	_AllGads,a1
	add.l	d0,a1

	move.w	XG_x(a1),gng_LeftEdge(a0)	;Copy gadget boundaries
	move.w	XG_y(a1),gng_TopEdge(a0)
	move.w	XG_Width(a1),gng_Width(a0)
	move.w	XG_Height(a1),gng_Height(a0)

	move.l	XG_Label(a1),gng_GadgetText(a0)

	move.l	_StdScreen,a2		;Screen font info
	move.l	sc_Font(a2),gng_TextAttr(a0)

	move.l	d1,gng_Flags(a0)

	move.l	_VisualInfo,gng_VisualInfo(a0)

	clr.l	gng_UserData(a0)

	movem.l	(a7)+,d0-1/a0-2
	rts

;--------------------------------------------------------------
; Entry:	d0 = orientation to start with
;	a0 = window title
; Exit:	Window setup begun.

_BeginWindow:
	movem.l	d0-2/a0-1,-(a7)

	move.b	d0,_CurOrientation		;Set orientation

	lea	_Panel,a1			;Set title
	move.l	a0,PAN_Title(a1)

	;Set window width to PixLength(Title) + 9 * PixLength("X")
	;This is the minimum value and might increase.

	moveq	#0,d1
	bsr	_PixLength
	move.l	d0,d2

	moveq	#9,d0
	jsr	calc_xlength
	add.w	d0,d2
	move.l	d2,PAN_Width(a1)

	move.l	d2,_MaxWidth		;Set MaxWidth to window width

	bsr	_PushColumn

	movem.l	(a7)+,d0-2/a0-1
	rts

;--------------------------------------------------------------
; Entry:	<void>
; Exit:	Window setup finished.

_EndWindow:
	bsr	_CalcPanelSize
	bra	_PopColumn

;--------------------------------------------------------------
; Entr:	<void>
; Exit:	New column started

_PushColumn:
	movem.l	d0-1/d7/a0-1,-(a7)

	move.l	_ExtGadPtr,d0		;a0=&ExtGadStack[ExtGadPtr]
	move.l	d0,d7
	mulu	#XG_SIZEOF,d7
	lea	_ExtGadStack,a0
	add.l	d7,a0
	lea	-XG_SIZEOF(a0),a1		;a1=&ExtGadStack[ExtGadPtr-1]

	;Initialise new column dimensions
	moveq	#0,d7
	move.b	_LBWidth,d7		;ExtGadStack[].x = LBWidth;
	move.w	d7,XG_x(a0)
	move.b	_TBWidth,d7		;ExtGadStack[].y = TBWidth;
	move.w	d7,XG_y(a0)
	clr.w	XG_Width(a0)
	clr.w	XG_Height(a0)

	move.b	_CurOrientation,d1		;d1=current orientation

	tst.l	d0			;Skip setup if first col
	ble	.pc_firstcol

	cmp.b	#HORIZONTAL,d1		;Currently horizontal?
	bne.s	.pc_curvert		;Skip if not

	;Current column is a horizontal

	move.w	XG_x(a1),d7		;col[].x = col[-1].x + col[-1].w
	add.w	XG_Width(a1),d7
	move.w	d7,XG_x(a0)

	move.w	XG_y(a1),XG_y(a0)		;col[].y = col[-1].y
	bra.s	.pc_firstcol

.pc_curvert:
	;Current column is a vertical

	move.w	XG_x(a1),XG_x(a0)		;col[].x = col[-1].x

	move.w	XG_y(a1),d7		;col[].y = col[-1].y + col[-1].h
	add.w	XG_Height(a1),d7
	move.w	d7,XG_y(a0)

.pc_firstcol:
	addq.l	#1,d0			;Increase stack pointer
	move.l	d0,_ExtGadPtr

	bchg	#0,d1			;Flip orientation
	move.b	d1,_CurOrientation

	movem.l	(a7)+,d0-1/d7/a0-1
	rts

;--------------------------------------------------------------
; Entr:	<void>
; Exit:	Current column finished off

_PopColumn:
	movem.l	d0-1/d6-7/a0-1,-(a7)

	move.b	_CurOrientation,d1		;Flip orientation
	bchg	#0,d1
	move.b	d1,_CurOrientation

	move.l	_ExtGadPtr,d0		;Pop from stack
	subq.l	#1,d0
	move.l	d0,_ExtGadPtr

	ble.s	.pc_firstcol		;Skip if first column

	move.l	d0,d7			;a0=&ExtGadStack[ExtGadPtr]
	mulu	#XG_SIZEOF,d7
	lea	_ExtGadStack,a0
	add.l	d7,a0
	lea	-XG_SIZEOF(a0),a1		;a1=&ExtGadStack[ExtGadPtr-1]

	cmp.b	#HORIZONTAL,d1		;Horiz or vertical now?
	bne.s	.pc_curvert		;Skip if vertical

	;Current orientation is horizontal
	move.w	XG_Width(a1),d7		;col[-1].w += col[].w
	add.w	XG_Width(a0),d7
	move.w	d7,XG_Width(a1)

	move.w	XG_Height(a0),d7		;col[-1].h = MAX(col[-1].h, col[].h)
	cmp.w	XG_Height(a1),d7
	ble.s	.pc_setmaxwidth
	move.w	d7,XG_Height(a1)
	bra.s	.pc_setmaxwidth

.pc_curvert:
	;Current orientation is vertical
	move.w	XG_Height(a1),d7		;col[-1].h += col[].h
	add.w	XG_Height(a0),d7
	move.w	d7,XG_Height(a1)

	;col[-1].w = MAX(col[-1].w, col[].w. MaxWidth)

	move.w	XG_Width(a0),d7		;d7=MAX(col[].w. MaxWidth)
	ext.l	d7
	move.l	_MaxWidth,d6
	cmp.l	d6,d7
	bge.s	.pc_cv1
	move.l	d6,d7

.pc_cv1:	cmp.w	XG_Width(a1),d7
	ble.s	.pc_setmaxwidth
	move.w	d7,XG_Width(a1)

.pc_setmaxwidth:
	move.w	XG_Width(a1),d7		;If current col is wider
	ext.l	d7
	cmp.l	_MaxWidth,d7		;than max, set new max.
	ble.s	.pc_gotmaxwidth
	move.l	d7,_MaxWidth

.pc_gotmaxwidth:
.pc_firstcol:

	movem.l	(a7)+,d0-1/d6-7/a0-1
	rts

;--------------------------------------------------------------
; Entry:	<void>
; Exit:	d0=pointer to current gadget stack entry

_GetColumnInfo:
	move.l	_ExtGadPtr,d0
	mulu	#XG_SIZEOF,d0
	add.l	#_ExtGadStack,d0
	rts

;--------------------------------------------------------------
; Entry:	<void>
; Exit:	Panel size calculated

_CalcPanelSize:
	movem.l	d0-1/d7/a0-1,-(a7)

	lea	_Panel,a0			;a0=panel structure
	lea	_ExtGadStack,a1		;a1=first column info

	move.w	XG_x(a1),d0		;d0=col.x+col.w+RBWidth
	add.w	XG_Width(a1),d0
	moveq	#0,d1
	move.b	_RBWidth,d1
	add.w	d1,d0
	ext.l	d0

	move.l	PAN_Width(a0),d7		;d0=max(d0,Panel.Width)
	cmp.l	d7,d0
	bge.s	.cp_skip1
	move.l	d7,d0
.cp_skip1:
	move.l	_MaxWidth,d7		;d0=max(d0,MaxWidth)
	cmp.l	d7,d0
	bge.s	.cp_skip2
	move.l	d7,d0
.cp_skip2:
	add.l	#INTERWIDTH,d0		;Set panel width
	add.l	d1,d0
	move.l	d0,PAN_Width(a0)


	move.w	XG_y(a1),d0		;Set panel height
	add.w	XG_Height(a1),d0
	add.w	#INTERHEIGHT,d0
	moveq	#0,d1
	move.b	_BBWidth,d1
	add.w	d1,d0
	ext.l	d0
	move.l	d0,PAN_Height(a0)

	movem.l	(a7)+,d0-1/d7/a0-1
	rts

;--------------------------------------------------------------
; Entr:	d0=gadget ID
; Exit:	Gadget added to stack

_GadgetAdd:
	movem.l	d1/d7/a0-1,-(a7)

	move.l	_ExtGadPtr,d7		;a0=&ExtGadStack[ExtGadPtr-1]
	mulu	#XG_SIZEOF,d7
	lea	_ExtGadStack-XG_SIZEOF,a0
	add.l	d7,a0

	move.l	d0,d7			;a1=&AllGads[GadgetID]
	mulu	#XG_SIZEOF,d7
	lea	_AllGads,a1
	add.l	d7,a1

	move.b	_CurOrientation,d1		;Skip on orientation
	cmp.b	#HORIZONTAL,d1
	bne	.ga_curvert

	;Current orientation is horizontal

	;AllGads[Gadget].x = ExtGadStack[ptr-1].x 
	;			+ ExtGadStack[ptr-1].Width + INTERWIDTH;
	move.w	XG_x(a0),d7
	add.w	XG_Width(a0),d7
	add.w	#INTERWIDTH,d7
	move.w	d7,XG_x(a1)

	;AllGads [Gadget].y = ExtGadStack [ptr - 1].y + INTERHEIGHT;
	move.w	XG_y(a0),d7
	add.w	#INTERHEIGHT,d7
	move.w	d7,XG_y(a1)

	;ExtGadStack [ptr-1].Width += AllGads [Gadget].Width + INTERWIDTH;
	move.w	XG_Width(a1),d7
	add.w	#INTERWIDTH,d7
	add.w	d7,XG_Width(a0)

	;ExtGadStack [ptr-1].Height = 
	;		MAX (ExtGadStack [ptr-1].Height, 
	;			AllGads [Gadget].Height + INTERHEIGHT);
	move.w	XG_Height(a1),d7
	add.w	#INTERHEIGHT,d7
	cmp.w	XG_Height(a0),d7
	ble.s	.ga_done
	move.w	d7,XG_Height(a0)
	bra.s	.ga_done

.ga_curvert:
	;Current orientation is horizontal

	;AllGads [Gadget].x = ExtGadStack [ptr-1].x + INTERWIDTH;
	move.w	XG_x(a0),d7
	add.w	#INTERWIDTH,d7
	move.w	d7,XG_x(a1)

	;AllGads [Gadget].y = 
	;  ExtGadStack [ptr-1].y + ExtGadStack [ptr-1].Height + INTERHEIGHT;
	move.w	XG_y(a0),d7
	add.w	XG_Height(a0),d7
	add.w	#INTERHEIGHT,d7
	move.w	d7,XG_y(a1)

	;ExtGadStack [ptr-1].Height += AllGads [Gadget].Height + INTERHEIGHT;
	move.w	XG_Height(a1),d7
	add.w	#INTERHEIGHT,d7
	add.w	d7,XG_Height(a0)

	;ExtGadStack [ptr-1].Width = 
	;	MAX (ExtGadStack [ptr-1].Width, 
	;		AllGads [Gadget].Width + INTERWIDTH);

	move.w	XG_Width(a1),d7
	add.w	#INTERWIDTH,d7
	cmp.w	XG_Width(a0),d7
	ble.s	.ga_done
	move.w	d7,XG_Width(a0)

.ga_done: movem.l	(a7)+,d1/d7/a0-1
	rts

;--------------------------------------------------------------
; Entr:	d0=gadget ID
; Exit:	Gadget aligned to right of current column

_AlignToRight:
	movem.l	d0/d7/a0-1,-(a7)

	mulu	#XG_SIZEOF,d0		;a0=&AllGads[GadgetID]
	lea	_AllGads,a0
	add.l	d0,a0

	move.l	_ExtGadPtr,d7		;a1=&ExtGadStack[ExtGadPtr-1]
	mulu	#XG_SIZEOF,d7
	lea	_ExtGadStack-XG_SIZEOF,a1
	add.l	d7,a1

	move.w	XG_Width(a1),d7
	sub.w	XG_Width(a0),d7
	add.w	#INTERWIDTH,d7
	move.w	d7,XG_x(a0)

	movem.l	(a7)+,d0/d7/a0-1
	rts
