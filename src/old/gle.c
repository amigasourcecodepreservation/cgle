/*
 * gle.c main file
*/

#define INTUI_V36_NAMES_ONLY

#include <clib/alib_protos.h>
#include <clib/asl_protos.h>
#include <clib/dos_protos.h>
#include <clib/exec_protos.h>
#include <clib/gadtools_protos.h>
#include <clib/graphics_protos.h>
#include <clib/icon_protos.h>
#include <clib/graphics_protos.h>
#include <clib/intuition_protos.h>
#include <clib/wb_protos.h>
#include <dos.h>
#include <exec/lists.h>
#include <exec/memory.h>
#include <exec/nodes.h>
#include <exec/types.h>
#include <intuition/gadgetclass.h>
#include <stdio.h>
#include <string.h>

#include "gle.h"

char Version[] = "$VER: Gadtools Layout Engine v1.00 (" __DATE__ ")";

/***** GLOBAL VARIABLES *****/

struct EXT_GADGET AllGads [GAD_MAXIMUM] = {NULL};
struct NewGadget NGad = {0};
struct PANEL Panel = {0};
struct Screen *StdScreen = NULL;

/***** INSTANCE VARIABLES *****/

static APTR *VisualInfo = NULL;

static BYTE LBWidth, RBWidth, TBWidth, BBWidth;
static WORD FontY;

// For the window layout engine

static struct EXT_GADGET ExtGadStack [20];
static LONG ExtGadPtr = 0;
static UBYTE CurOrientation;
static ULONG MaxWidth = 0;

/***** EXTERNAL VARIABLES *****/

extern struct Library *IntuitionBase;

/*	This function sets up all necessary system variables for screen
	and window use.
*/

BOOL WindowConstructor (void)
{
	/* Get information about current screen
    */
	StdScreen = LockPubScreen (NULL);
	if (!StdScreen) return FALSE;

	FontY 	= StdScreen->RastPort.TxHeight;
	LBWidth = StdScreen->WBorLeft;
	RBWidth = StdScreen->WBorRight;
	TBWidth = StdScreen->WBorTop + FontY + 1;
	BBWidth = StdScreen->WBorBottom;

	VisualInfo = GetVisualInfo (StdScreen, TAG_END);
	if (!VisualInfo) return FALSE;

	return TRUE;
}


/*	This function frees all screen-related system structures.
*/

void WindowDestructor (void)
{
	/* Close our window and free the gadgets
	*/
	CloseControlPanel();

	if (Panel.GList)
	{
		FreeGadgets (Panel.GList);
		Panel.GList = NULL;
	}

	if (VisualInfo)
	{
		FreeVisualInfo (VisualInfo);
		VisualInfo = NULL;
	}

	if (StdScreen)
	{
		UnlockPubScreen (NULL, StdScreen);
		StdScreen = NULL;
	}
}


/***** REQUESTERS *****/

/*	This function asks the user a question.

	in: Title: the title of the requester.
	in: Question: the messagebody shown in the requester.
	in: Answers: the various possible responses.

	out: the selected response. Similar to EasyRequest ().
*/

LONG RequestUserInput (char *Title, char *Message, char *Responses)
{	LONG Result;
	struct EasyStruct MessageStruct;

	MessageStruct.es_StructSize = sizeof (struct EasyStruct);
	MessageStruct.es_Flags = 0;
	MessageStruct.es_Title = Title;
	MessageStruct.es_TextFormat = Message;
	MessageStruct.es_GadgetFormat = Responses;

	Result = EasyRequest (Panel.Win, &MessageStruct, 0);

	return Result;
}



/*	This function reports an error.
*/

void ReportError (WORD Level, char *Message)
{	switch (Level) {
		case WARN:
			RequestUserInput ("Warning", Message, "Okay");
			break;
		case ERROR:
			RequestUserInput ("Error", Message, "Okay");
			break;
		case BUG:
			RequestUserInput ("Bug", Message, "Okay");
			break;
	}
}


/*	This function returns the length in pixels for a given string
	and returns the result.
*/

LONG PixLength (char *Text, LONG OldLength)
{	WORD NewLength;

	NewLength = TextLength (&StdScreen->RastPort, Text, (ULONG)strlen (Text));

	return MAX (NewLength, OldLength);
}




/*	This function returns the length of a label in pixels. It compensates
	for the extra space left and right of the label and filters out the
	underscore character, '_'.
*/

LONG LabelPixLength (char *Text)
{	char Label [64];
	LONG st, dt;

	st = 0;
	dt = 0;

	while (Text [st]) {
		if (Text [st] != '_')
			Label [dt++] = Text [st];

		st++;
	}

	Label [dt++] = 0;

	if (dt > 63) ReportError (ERROR, "LabelPixLength: internal buffer overflow.");

	return PixLength (Label, 0);
}




/*	This function sets up values for a gadget.
*/

void SetNewGad (ULONG Gadget, ULONG Flags)
{	NGad.ng_LeftEdge	= AllGads [Gadget].x;
	NGad.ng_TopEdge		= AllGads [Gadget].y;
	NGad.ng_Width		= AllGads [Gadget].Width;
	NGad.ng_Height		= AllGads [Gadget].Height;
	NGad.ng_GadgetText	= AllGads [Gadget].Label;
	NGad.ng_TextAttr	= StdScreen->Font;
	NGad.ng_GadgetID	= Gadget;
	NGad.ng_Flags		= Flags;
	NGad.ng_VisualInfo	= VisualInfo;
	NGad.ng_UserData	= NULL;
}


/*	This function opens the control basic panels
*/

BOOL OpenControlPanel (void)
{	WORD ZoomSize [4];

	/* If possible, bring an old window to front */

	if (Panel.Win)
	{
		WindowToFront (Panel.Win);
		ActivateWindow (Panel.Win);
		return TRUE;
	}

	/* Open a new window */
	ZoomSize [0] = 0;
	ZoomSize [1] = 0;
	ZoomSize [2] = 5 * PixLength ("XXXXX", 0);
	ZoomSize [3] = TBWidth;

	Panel.Width = MIN(Panel.Width, StdScreen->Width);
	Panel.Height= MIN(Panel.Height, StdScreen->Height);

	Panel.WinX	= ((StdScreen->Width - Panel.Width) / 2);
	Panel.WinY	= ((StdScreen->Height - Panel.Height) / 2);

	Panel.Win = OpenWindowTags (NULL,
					WA_Left, Panel.WinX,
					WA_Top, Panel.WinY,
					WA_Width, Panel.Width,
					WA_Height, Panel.Height,
					WA_CloseGadget, TRUE,
					WA_DepthGadget, TRUE,
					WA_DragBar, TRUE,
					WA_Gadgets, Panel.GList,
					WA_Zoom, ZoomSize,
					WA_PubScreen, StdScreen,
					WA_Activate, TRUE,
					WA_SimpleRefresh, TRUE,
					WA_NewLookMenus, TRUE,
					WA_IDCMP, 0L,
					WA_RMBTrap, TRUE,
					WA_Title, Panel.Title,
					TAG_DONE);

	if (!Panel.Win) return FALSE;

	Panel.Win->UserData = (void *)(&Panel);

	ModifyIDCMP (Panel.Win, IDCMP_CLOSEWINDOW |
							SLIDERIDCMP |
							MXIDCMP |
							BUTTONIDCMP |
							STRINGIDCMP |
							CYCLEIDCMP |
							IDCMP_MENUPICK |
							IDCMP_MOUSEMOVE |
							IDCMP_VANILLAKEY |
							IDCMP_CHANGEWINDOW |
							IDCMP_REFRESHWINDOW);

	GT_RefreshWindow (Panel.Win, NULL);

	return TRUE;
}




/*	This function removes the control panel from memory.
*/

void CloseControlPanel (void)
{
	if (Panel.Win)
	{
		ClearMenuStrip (Panel.Win);
		CloseWindowSafely (Panel.Win);
		Panel.Win = NULL;
	}
}




/*	This function closes a window with a shared IDCMP. Taken from
	Amiga RKM 3rd edition.
*/

void CloseWindowSafely (struct Window *Win)
{	struct IntuiMessage *msg;
	struct Node *succ;

	Forbid ();

	/* remove all messages for our window */

	msg = (struct IntuiMessage *)Win->UserPort->mp_MsgList.lh_Head;

	while (succ = msg->ExecMessage.mn_Node.ln_Succ) {
		if (msg->IDCMPWindow == Win) {
			Remove ((struct Node *)msg);
			ReplyMsg ((struct Message *)msg);
		}
		msg = (struct IntuiMessage *)succ;
	}

	Win->UserPort = NULL;

	ModifyIDCMP (Win, 0L);

	Permit ();

	CloseWindow (Win);
}




/*	The following functions perform operations on gadgets.
*/




void GadgetEnable (ULONG Gadget)
{	GT_SetGadgetAttrs (AllGads [Gadget].Gad, Panel.Win, NULL,
		GA_Disabled, FALSE,
		TAG_DONE);

	AllGads [Gadget].Disabled = 0;
}

void GadgetDisable (ULONG Gadget)
{	GT_SetGadgetAttrs (AllGads [Gadget].Gad, Panel.Win, NULL,
		GA_Disabled, TRUE,
		TAG_DONE);

	AllGads [Gadget].Disabled = 1;
}

void GadgetActivate (ULONG Gadget)
{	if (!AllGads [Gadget].Disabled)
		ActivateGadget (AllGads [Gadget].Gad, Panel.Win, NULL);
}




/*	This function sets a checkbox to a specified state.
*/

void SetCheckbox (ULONG Gadget, UWORD State)
{
	GT_SetGadgetAttrs (AllGads [Gadget].Gad, Panel.Win, NULL,
		GTCB_Checked, State,
		TAG_END);
}


/*	This function sets a cycle gadget to a specified state.
*/

void SetCycle (ULONG Gadget, UWORD State)
{	GT_SetGadgetAttrs (AllGads [Gadget].Gad, Panel.Win, NULL,
		GTCY_Active, State,
		TAG_END);
}




/*	This function sets a text gadget to a specified string.
*/

void SetText (ULONG Gadget, char *Text)
{	GT_SetGadgetAttrs (AllGads [Gadget].Gad, Panel.Win, NULL,
		GTTX_Text, Text,
		TAG_END);
}




/*	This function sets a number gadget to a specified value.
*/

void SetNumber (ULONG Gadget, LONG Value)
{	GT_SetGadgetAttrs (AllGads [Gadget].Gad, Panel.Win, NULL,
		GTNM_Number, Value,
		TAG_END);
}




/*	This function sets a string gadget to a specified string.
*/

void SetString (ULONG Gadget, char *Text)
{	GT_SetGadgetAttrs (AllGads [Gadget].Gad, Panel.Win, NULL,
		GTST_String, Text,
		TAG_END);
}




/*	This function sets a slider to a specified value.
*/

void SetSlider (ULONG Gadget, LONG Value)
{	GT_SetGadgetAttrs (AllGads [Gadget].Gad, Panel.Win, NULL,
		GTSL_Level, Value,
		TAG_END);
}




/*	This function sets a radiobutton to a specified value.
*/

void SetRadio (ULONG Gadget, UBYTE Value)
{	GT_SetGadgetAttrs (AllGads [Gadget].Gad, Panel.Win, NULL,
		GTMX_Active, Value,
		TAG_END);
}




/*	This group of functions calculate the minimum size taken by a
	specific kind of gadget.
*/

void CalcCheckbox (ULONG Gadget)
{
	AllGads [Gadget].x = 0;
	AllGads [Gadget].y = 0;
	AllGads [Gadget].Width = PixLength ("xxx", 0);
	AllGads [Gadget].Height = FontY + 4;
}

void CalcCycle (ULONG Gadget, char **ItemList)
{	LONG t, Width;

	t = 0;
	Width = 0;

	while (ItemList [t]) {
		Width = PixLength (ItemList [t], Width);
		t++;
	}

	Width += PixLength ("xxxx", 0);

	AllGads [Gadget].x = 0;
	AllGads [Gadget].y = 0;
	AllGads [Gadget].Width = Width;
	AllGads [Gadget].Height = FontY + 4;
}

void CalcText (ULONG Gadget, char *Text)
{	AllGads [Gadget].x = 0;
	AllGads [Gadget].y = 0;
	AllGads [Gadget].Width = PixLength (Text, 0) + PixLength ("xx", 0);
	AllGads [Gadget].Height = FontY + 4;
}

void CalcNumber (ULONG Gadget, LONG Value)
{	char Text [12];

	sprintf (Text, " %d ", Value);

	AllGads [Gadget].x = 0;
	AllGads [Gadget].y = 0;
	AllGads [Gadget].Width = PixLength (Text, 0);
	AllGads [Gadget].Height = FontY + 4;
}

void CalcList (ULONG Gadget, LONG Lines, struct List *ItemList)
{	LONG Width;
	struct Node *CurNode;

	Width = 0;

	for (CurNode = ItemList->lh_Head; CurNode->ln_Succ; CurNode = CurNode->ln_Succ)
		Width = PixLength (CurNode->ln_Name, Width);

	Width += PixLength ("xxxx", 0);

	AllGads [Gadget].Label = NULL;
	AllGads [Gadget].x = 0;
	AllGads [Gadget].y = 0;
	AllGads [Gadget].Width = Width;
	AllGads [Gadget].Height = (FontY + 4 + INTERHEIGHT) * Lines - INTERHEIGHT;
}

void CalcString (ULONG Gadget, char *Text)
{	AllGads [Gadget].x = 0;
	AllGads [Gadget].y = 0;
	AllGads [Gadget].Width = PixLength (Text, 0) + PixLength ("xx", 0);
	AllGads [Gadget].Height = FontY + 4;
}

void CalcButton (ULONG Gadget, char *Label)
{	AllGads [Gadget].Label = Label;
	AllGads [Gadget].x = 0;
	AllGads [Gadget].y = 0;
	AllGads [Gadget].Width = LabelPixLength (Label) + PixLength ("xx", 0);
	AllGads [Gadget].Height = FontY + 4;
}

void CalcHSlider (ULONG Gadget, char *Text)
{	AllGads [Gadget].x = 0;
	AllGads [Gadget].y = 0;
	AllGads [Gadget].Width = PixLength (Text, 0);
	AllGads [Gadget].Height = FontY + 4;
}

void CalcVRadio (ULONG Gadget, char **ItemList)
{	LONG t, Width;

	t = 0;
	Width = 0;

	while (ItemList [t]) {
		Width = PixLength (ItemList [t], Width);
		t++;
	}

	Width += PixLength ("xxx", 0);

	AllGads [Gadget].Label = NULL;
	AllGads [Gadget].x = 0;
	AllGads [Gadget].y = 0;
	AllGads [Gadget].Width = Width;
	AllGads [Gadget].Height = (FontY + 4 + INTERHEIGHT) * t - INTERHEIGHT;
}

void CalcLabel (ULONG Gadget, char *Label)
{	AllGads [Gadget].Label = Label;
	AllGads [Gadget].x = 0;
	AllGads [Gadget].y = 0;
	AllGads [Gadget].Width = LabelPixLength (Label);
	AllGads [Gadget].Height = FontY + 4;
}

void CalcBlank (ULONG Gadget, char *Text)
{	AllGads [Gadget].Label = NULL;
	AllGads [Gadget].x = 0;
	AllGads [Gadget].y = 0;
	AllGads [Gadget].Width = LabelPixLength (Text);
	AllGads [Gadget].Height = FontY + 4;
}




/*	The following routines together form the window layout engine.
*/

void BeginWindow (UBYTE Orientation, STRPTR Title)
{	CurOrientation = Orientation;

	Panel.Title = Title;
	Panel.Width = PixLength(Title, 0) + 9 * PixLength("X", 0);
	MaxWidth    = Panel.Width;
	PushColumn ();
}

void EndWindow (void)
{	CalcPanelSize ();
	PopColumn ();
}

void PushColumn (void)
{	ExtGadStack [ExtGadPtr].x = LBWidth;
	ExtGadStack [ExtGadPtr].y = TBWidth;
	ExtGadStack [ExtGadPtr].Width = 0;
	ExtGadStack [ExtGadPtr].Height = 0;

	if (ExtGadPtr > 0) {
		switch (CurOrientation) {
			case HORIZONTAL:
				ExtGadStack [ExtGadPtr].x = ExtGadStack [ExtGadPtr - 1].x + ExtGadStack [ExtGadPtr - 1].Width;
				ExtGadStack [ExtGadPtr].y = ExtGadStack [ExtGadPtr - 1].y;
				break;
			case VERTICAL:
				ExtGadStack [ExtGadPtr].x = ExtGadStack [ExtGadPtr - 1].x;
				ExtGadStack [ExtGadPtr].y = ExtGadStack [ExtGadPtr - 1].y + ExtGadStack [ExtGadPtr - 1].Height;
				break;
			default:
				ReportError (BUG, "PushColumn: bad orientation.");
		}
	}

	ExtGadPtr++;
	CurOrientation = !CurOrientation;

	if (ExtGadPtr > 19) ReportError (BUG, "PushColumn: stack too small.");
}

void PopColumn (void)
{	ExtGadPtr--;
	CurOrientation = !CurOrientation;

	if (ExtGadPtr > 0)
	{
		switch (CurOrientation)
		{
			case HORIZONTAL:
				ExtGadStack [ExtGadPtr - 1].Width += ExtGadStack [ExtGadPtr].Width;
				ExtGadStack [ExtGadPtr - 1].Height = MAX (ExtGadStack [ExtGadPtr - 1].Height, ExtGadStack [ExtGadPtr].Height);
				break;
			case VERTICAL:
				ExtGadStack [ExtGadPtr - 1].Width   = MAX(MaxWidth, MAX(ExtGadStack[ExtGadPtr - 1].Width, ExtGadStack[ExtGadPtr].Width));
				ExtGadStack [ExtGadPtr - 1].Height += ExtGadStack [ExtGadPtr].Height;
				break;
			default:
				ReportError (BUG, "PopColumn: bad orientation.");
		}

		if (ExtGadStack [ExtGadPtr - 1].Width > MaxWidth)
		{
			MaxWidth = ExtGadStack [ExtGadPtr - 1].Width;
		}
	}
}

struct EXT_GADGET *GetColumnInfo (void)
{	return &ExtGadStack [ExtGadPtr];
}

void GadgetAdd (ULONG Gadget)
{	switch (CurOrientation) {
		case VERTICAL:
			AllGads [Gadget].x = ExtGadStack [ExtGadPtr - 1].x + INTERWIDTH;
			AllGads [Gadget].y = ExtGadStack [ExtGadPtr - 1].y + ExtGadStack [ExtGadPtr - 1].Height + INTERHEIGHT;

			ExtGadStack [ExtGadPtr - 1].Width = MAX (ExtGadStack [ExtGadPtr - 1].Width, AllGads [Gadget].Width + INTERWIDTH);
			ExtGadStack [ExtGadPtr - 1].Height += AllGads [Gadget].Height + INTERHEIGHT;
			break;

		case HORIZONTAL:
			AllGads [Gadget].x = ExtGadStack [ExtGadPtr - 1].x + ExtGadStack [ExtGadPtr - 1].Width + INTERWIDTH;
			AllGads [Gadget].y = ExtGadStack [ExtGadPtr - 1].y + INTERHEIGHT;

			ExtGadStack [ExtGadPtr - 1].Width += AllGads [Gadget].Width + INTERWIDTH;
			ExtGadStack [ExtGadPtr - 1].Height = MAX (ExtGadStack [ExtGadPtr - 1].Height, AllGads [Gadget].Height + INTERHEIGHT);
			break;

		default:
			ReportError (BUG, "GadgetAdd: bad orientation.");
	}
}

void AlignToRight (ULONG Gadget)
{	AllGads [Gadget].x = ExtGadStack [ExtGadPtr - 1].Width - AllGads [Gadget].Width + INTERWIDTH;
}

void CalcPanelSize (void)
{	if (ExtGadPtr != 1) ReportError (BUG, "CalcPanelSize: wrong stack position.");

	Panel.Width  = MAX(MaxWidth, MAX(Panel.Width, ExtGadStack [0].x + ExtGadStack [0].Width + RBWidth)) + INTERWIDTH + RBWidth;
	Panel.Height = ExtGadStack [0].y + ExtGadStack [0].Height + INTERHEIGHT + BBWidth;
}

void CreateCheckbox (struct Gadget **MyGadget, ULONG GadNumber)
{	SetNewGad (GadNumber, PLACETEXT_LEFT);

	*MyGadget = CreateGadget (CHECKBOX_KIND, *MyGadget, &NGad,
			GT_Underscore, '_',
			GTCB_Scaled, TRUE,
			TAG_DONE);
	AllGads [GadNumber].Gad = *MyGadget;
}

void CreateCycle (struct Gadget **MyGadget, ULONG GadNumber, STRPTR *Labels)
{	SetNewGad (GadNumber, PLACETEXT_LEFT);
	*MyGadget = CreateGadget (CYCLE_KIND, *MyGadget, &NGad,
			GT_Underscore, '_',
			GTCY_Labels, Labels,
			TAG_DONE);
	AllGads [GadNumber].Gad = *MyGadget;
}

void CreateString (struct Gadget **MyGadget, ULONG GadNumber, ULONG MaxChars)
{	SetNewGad (GadNumber, PLACETEXT_LEFT);
	*MyGadget = CreateGadget (STRING_KIND, *MyGadget, &NGad,
			GT_Underscore, '_',
			GTST_MaxChars, MaxChars,
			TAG_DONE);
	AllGads [GadNumber].Gad = *MyGadget;
}

void CreateButton (struct Gadget **MyGadget, ULONG GadNumber)
{	SetNewGad (GadNumber, PLACETEXT_IN);
	*MyGadget = CreateGadget (BUTTON_KIND, *MyGadget, &NGad,
			GT_Underscore, '_',
			TAG_END);
	AllGads [GadNumber].Gad = *MyGadget;
}

void CreateLabel (struct Gadget **MyGadget, ULONG GadNumber)
{	SetNewGad (GadNumber, PLACETEXT_IN);
	*MyGadget = CreateGadget (TEXT_KIND, *MyGadget, &NGad,
			GT_Underscore, '_',
			TAG_END);
	AllGads [GadNumber].Gad = *MyGadget;
}

void CreateText (struct Gadget **MyGadget, ULONG GadNumber)
{	SetNewGad (GadNumber, PLACETEXT_IN);
	*MyGadget = CreateGadget (TEXT_KIND, *MyGadget, &NGad,
			GT_Underscore, '_',
			GTTX_Border, TRUE,
			TAG_END);
	AllGads [GadNumber].Gad = *MyGadget;
}

void CreateNumber (struct Gadget **MyGadget, ULONG GadNumber)
{	SetNewGad (GadNumber, PLACETEXT_IN);
	*MyGadget = CreateGadget (NUMBER_KIND, *MyGadget, &NGad,
			GT_Underscore, '_',
			GTNM_Border, TRUE,
			TAG_END);
	AllGads [GadNumber].Gad = *MyGadget;
}


